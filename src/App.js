import React,{useEffect} from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import './App.css';
import './component/js/Header'
import Header from './component/js/Header';
import Checkout from './component/js/Checkout'
import Home from './component/js/Home'
import Login from './component/js/Login';
import Register from './component/js/RegisterUser';
import { StateConsumer } from './component/js/StateProvider';
import PlaceOrder from './component/js/PlaceOrder';


function App() {
  const [{user,basket}, dispatch] = StateConsumer()
  useEffect(()=>{
    let isActive = localStorage.getItem('isActive')
    let user = localStorage.getItem('user')
    if(isActive==='true' && user!==null){
     dispatch({
       type: 'SET_USER',
       user:localStorage.getItem('user')
     })
    }else{
       if(isActive===false && user===null){
        dispatch({
          type: 'SET_USER',
          user:null
       })
     }
    }
  },[user,basket,dispatch])
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path='/login'>
            <Login />
          </Route>
          <Route path='/checkout'>
            <Header />
            <Checkout />
          </Route>
          <Route path='/register'>
              <Register/>
          </Route>
          <Route path='/order'>
            <Header />
            <PlaceOrder />
          </Route>
          <Route path='/'>
            <Header />
            <Home/>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
