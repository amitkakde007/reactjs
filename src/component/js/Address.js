import React from 'react'
import '../css/Address.css'


function Address(props){
    return(
        <div className='user_address'>
            <li>{props.address}</li>
            <li>{props.city}, {props.state}{props.pincode}</li>
            <li>India</li>
            <div className='address__edit'>
                <button className='deliver__button'>Deliver to this address</button>
                <button>Edit</button> 
                <button>Delete</button>
            </div>
        </div>
    )
}

export default Address