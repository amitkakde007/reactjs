import React,{useEffect, useState} from 'react'
import '../css/PlaceOrder.css'
import axiosInstance from './axios'
import Address from '../js/Address'

function PlaceOrder() {
    const loggedInUser = localStorage.getItem('user')
    const [addressData, setAddressData] = useState([])
    let address

    useEffect(()=> {
        axiosInstance.get(`user/address/${loggedInUser}`,{
        }).then(resp=> resp.data)
        .then(data => setAddressData(data))
    },
    [loggedInUser])
    address = addressData.map(item=>(
        <Address 
            address = {item.address} 
            city = {item.city}
            state = {item.state}
            pincode = {item.pincode}
        />
    ))
    return (
        <div className='delivery__details'>
            <h4>Select a delivery address</h4>
            <div className='address__card'>
                {address}
            </div>
        </div>
    )
}

export default PlaceOrder
