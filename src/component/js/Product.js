import React from 'react'
import '../css/Product.css'
import {StateConsumer} from '../js/StateProvider'

function Product(props){
    const [,dispatch] = StateConsumer()
    const addToBasket=()=>{
        dispatch({
            type: 'ADD_TO_BASKET',
            item:{
                title:props.title,
                price:props.price,
                rating:props.rating,
                image:props.img,
            }
        })
    }
    return(
        <div className='product'>
            <div className='product__detail'>
                <p>{props.title}</p>
                <p className='product__price'>
                    <small> ₹ </small>
                    <strong>{props.price}</strong>
                </p>
                <div className="product__rating">
                    {Array(props.rating).fill().map((_,i)=><span role='img' aria-label='star__rating'>🌟</span>)}
                </div>
            </div>
            <img src={props.img} alt='' />
            <button onClick={addToBasket}>Add to basket</button>
        </div>
    )
}

export default Product