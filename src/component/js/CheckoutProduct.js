import React from 'react'
import '../css/CheckoutProduct.css'
import { StateConsumer } from './StateProvider'


function CheckoutProduct(props) {
    const [,dispatch] = StateConsumer()

    const removeFromBasket =() =>{
            dispatch({
                type: 'REMOVE_FROM_BASKET',
                item:{
                    title: props.title,
                    price: props.price,
                    image: props.img,
                    rating: props.rating,
                }
            })
    }
    return (
        <div className='checkout__product'>
            <div className='product__image'>
                <img src={props.img} alt='' />
            </div>
            <div className='product__details'>
                <strong>{props.title}</strong>
                <p className='product__rating'>{Array(props.rating).fill().map(()=><span role='img' aria-label='star__rating'>🌟</span>)}</p>
                <p><button onClick={removeFromBasket}>Remove from basket</button></p>
            </div>
            <div className='product__price'>
                <small>₹ </small>
                <span>{props.price}</span>
            </div>
        </div>
    )
}

export default CheckoutProduct
