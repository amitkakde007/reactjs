import React from 'react'
import CurrencyFormat from 'react-currency-format'
import '../css/Checkout.css'
import Subtotal from '../js/Subtotal'
import CheckoutProduct from './CheckoutProduct'
import { StateConsumer, getBasketTotalValue } from './StateProvider'

function Checkout() {
    const [{ basket }] = StateConsumer()
    return (
        <div className="checkout">
            <div className='checkout__list'>
                <img className='checkout__ad'
                    src='https://images-na.ssl-images-amazon.com/images/G/02/UK_CCMP/TM/OCC_Amazon1._CB423492668_jpg' alt='' />
                <div className="checkout__details">
                    <h1 className='checkout__title'>Shopping Cart</h1>
                    <small>Price</small>
                </div>
                <div>
                    {basket.map(item =>
                        <CheckoutProduct
                            title={item.title}
                            price={item.price}
                            rating={item.rating}
                            img={item.image}
                        />
                    )}
                    <CurrencyFormat
                        renderText={(value) => basket.length!==0 ?(
                            <h3 className="subtotal__value">
                                Subtotal ({basket.length} items):<h3>{value}</h3>
                            </h3>
                        ):<h3>Feels empty here !!!</h3>}
                        decimalScale={2}
                        value={getBasketTotalValue(basket)}
                        displayType={'text'}
                        thousandSeparator={true}
                        prefix={'₹ '}
                    />
                </div>
            </div>
            <div className='checkout__subtotal'>
                <Subtotal />
            </div>
        </div>
    )
}

export default Checkout
