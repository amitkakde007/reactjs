import React, {createContext, useReducer,useContext} from 'react'

const StateContext = createContext()

export const initialState={
    basket:[],
    user:null
}

//Selectors
export const getBasketTotalValue = (basket) => basket?.reduce((amount,item) => item.price + amount,0)

export const reducer = (state, action)=>{
    switch(action.type){
        case 'ADD_TO_BASKET':
            return{
                ...state,
                basket:[...state.basket, action.item]
            }
        case 'REMOVE_FROM_BASKET':
            const index = state.basket.findIndex(
                (basketItem)=> basketItem.id === action.id
            )
            let newBasket = [...state.basket]

            if (index>=0){
                newBasket.splice(index,1)
            }else{
                console.warn(`Cannot remove item ${action.id}`)
            }
            return{
                ...state,
                basket:newBasket
            }
        case 'SET_USER':
            return{
                ...state,
                user:action.user
            }
        default:
            return state
    }
}

export const StateProvider=({reducer, initialState, children})=>(
    <StateContext.Provider value= {useReducer(reducer, initialState)}>
        {children}
    </StateContext.Provider>
)

export const StateConsumer = ()=> useContext(StateContext)