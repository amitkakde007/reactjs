import React from 'react'
import {Link, useHistory} from 'react-router-dom'
import '../css/Header.css'
import SearchIcon from '@material-ui/icons/Search';
import ShoppingBasketIcon from '@material-ui/icons/ShoppingBasket';
import { StateConsumer } from './StateProvider';
import axiosInstance from './axios';

function Header() {
    const history = useHistory()
    const [{basket,user},dispatch] = StateConsumer()
    const logOut=()=>{ 
        if(user){
            axiosInstance.post('user/logout/',{
                refresh:localStorage.getItem('refresh')
            }).then(resp=>{
                if (resp.status===205){
                    localStorage.removeItem('access')
                    localStorage.removeItem('refresh')
                    localStorage.removeItem('user')
                    localStorage.setItem('isActive',false)
                    axiosInstance.defaults.headers['Authorization'] = null
                    history.push('/home')
                }
                dispatch({
                    type:'SET_USER',
                    user:null,
                })
            })
        }
    }
    return (
        <div className='header'>
            <Link to='/home'>
                <img className='header__logo' src="http://pngimg.com/uploads/amazon/amazon_PNG11.png" alt=""/>
            </Link>

            <div className="header__searchBar">
                <input className='header__searchIn' type='text'></input>
                <SearchIcon className='header__searchIcon'></SearchIcon>
            </div>

            <div className="header__nav">
                <Link to={!user && '/login'}>
                    <div onClick={logOut} className="header__option">
                        <span className='header__optionLineOne'/>Hello {user? user:'Guest'}
                        <span className='header__optionLineTwo'/>{user? 'Sign out': 'Sign in ?'}
                    </div>
                </Link>
                
                <div className="header__option">
                    <span className='header__optionLineOne'/>Return
                    <span className='header__optionLineTwo' />& orders
                </div>
                
                <div className="header__option">
                    <span className='header__optionLineOne'/>Your 
                    <span className='header__optionLineTwo'/>prime
                </div>
                
                <Link to='/checkout'>
                    <div className='header__optionBasket'>
                        <ShoppingBasketIcon />
                        <span className='header__optionLineTwo header__basketCount'>
                            {basket.length}
                        </span>
                    </div>
                </Link>
            </div>
        </div>    
    )
}

export default Header