import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import '../css/Home.css'
import Product from '../js/Product'
//import axios from 'axios'
import axiosInstance from './axios'


function Home() {
    const history = useHistory()
    const [data, setData] = useState([])
    let products
    useEffect(() => {
        axiosInstance.get('product/product-viewset/')
            .then(resp => setData(resp.data))
            .catch(error=>history.push('/login'))
    }
    ,[history])

    products = (data.map(item =>
        <div className="home__row">
            <Product 
                title={item.name} 
                price={item.price} 
                rating={item.rating}
                img={item.product_image.map(ele=>ele.image)}/>
        </div>
    ))
    return (
        <div className='home'>
            <div className='home__container'>
                <img className='home__image'
                src="https://images-eu.ssl-images-amazon.com/images/G/31/img20/Events/jupiter20/GWPhase1/5_DesktopHero_Unrec_150x600-._CB419216629_.jpg"
                alt=""/>  
                    {products}
            </div>
        </div>
    )
}

export default Home
