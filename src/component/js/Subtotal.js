import React from 'react'
import CurrencyFormat from 'react-currency-format'
import {useHistory} from 'react-router-dom'
import '../css/Subtotal.css'
import { getBasketTotalValue, StateConsumer } from './StateProvider';

function Subtotal() {
    const [{basket}] = StateConsumer()
    const history = useHistory()

    const checkoutOrder = e=>{
        const isActive=localStorage.getItem('isActive')
        if (isActive==='true'){
            if(basket.length!==0){
                history.push('/order')
            }else{
            alert('Your shopping cart is empty!!!')
        }}else{
            history.push('/login')
        }
    }
    return (
        <div className='subtotal'>
            <CurrencyFormat 
                renderText={(value)=>(
                    <>
                        <p>
                            Subtotal ({basket.length} items): <strong>{value}</strong>
                        </p>
                        <small className='subtotal__gift'>
                        <input type='checkbox'></input>This order contains a gift
                        </small>
                    </>
                )}
                decimalScale={2}
                value={getBasketTotalValue(basket)}
                displayType={'text'}
                thousandSeparator={true}
                prefix={'₹ '}
            />
            <button onClick={checkoutOrder}>Proceed to Buy</button>
        </div>
    );
}

export default Subtotal;
