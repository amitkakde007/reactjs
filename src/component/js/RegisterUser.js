    import React,{useState} from 'react'
import '../css/RegisterUser.css'
import {Link, useHistory} from 'react-router-dom'
import axiosInstance from './axios'

function Register() {
    const history = useHistory()
    const [email, setEmail] = useState('')
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [password2, setPassword2] = useState()

    const login=e=>{
        history.push('/login')
    }
    const signUp=e=>{
        e.preventDefault()
        if(password===password2){
            axiosInstance.post('user/register-viewset/',{
                email:email,
                username:username,
                password:password,
                confirm_password:password2,
            }).then(resp=>(resp.status===200)? history.push('/login'): alert(resp.statusText))
            .catch(error=>console.log(error))
        }else{
            alert('Password do not match!')
            setPassword('')
            setPassword2('')
        }
    }
    return (
        <div className='register'>
            <Link to='/register'>
                <img className='register__logo' 
                    src='https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Amazon_logo.svg/1024px-Amazon_logo.svg.png' alt='' />
            </Link>
            <div className='register__container'>
                <h1>Sign up</h1>
                <form>
                    <h5>Email Id</h5>
                    <input className='input__email' type='email' value={email} onChange={(e)=>setEmail(e.target.value)}></input>
                    <h5>Username</h5>
                    <input className='input__email' type='text' value={username} onChange={(e)=>setUsername(e.target.value)}></input>
                    <h5>Password</h5>
                    <input className='input__email' type='password' value={password} onChange={(e)=>setPassword(e.target.value)}></input>
                    <h5>Confirm Password</h5>
                    <input className='input__email' type='password' value={password2} onChange={(e)=>setPassword2(e.target.value)}></input>
                    <button className='register__button' type='submit' onClick={signUp}>Sign up</button>
                    <p><small>By continuing, you agree to Amazon's <a href='#top'>Conditions of use</a> and <a href='#top'>privacy Notice</a></small></p>
                </form>
            </div>
            <div className='divider'>
                <h5>Already have an account?</h5>
                <p className='divider__line'></p>
                <p><button onClick={login} className='login__button'>Log In to an existing account</button></p>
            </div>
        </div>
    )
}

export default Register
