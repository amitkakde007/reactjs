import axios from 'axios'

const axiosInstance = axios.create(
    {
        baseURL: 'http://localhost:8000/',
        timeout: 5000,
        headers: {
            'Authorization': localStorage.getItem('access')? 'JWT ' + localStorage.getItem('access'): null,
            'Content-type': 'application/json',
            'accept': 'application/json',
        }
    }
)

axiosInstance.interceptors.response.use(
    response => {
        return response
    },
    async error => {
        const prevRequest = error.config
        if (error.response.status===401 && error.response.statusText==='Unauthorized' && error.config.url==='user/api/token/'){
            console.log(error.response)
        }
        else if (error.response.status===401 && error.response.statusText==='Unauthorized' && error.response.data.code==='token_not_valid'){
            const refreshToken = localStorage.getItem('refresh')
            if (refreshToken){
                const validity = JSON.parse(atob(refreshToken.split('.')[1]))
                const now = (Math.ceil(Date.now()/1000))
                if(validity.exp>now){
                    return axiosInstance.post('user/api/token/refresh/',{
                        refresh:refreshToken
                    }).then(response=>{
                        const {access,refresh} = response.data
                        localStorage.setItem('access',access)
                        localStorage.setItem('refresh',refresh)
                        axiosInstance.defaults.headers['Authorization']= 'JWT '+ access
                        prevRequest.headers['Authorization']= 'JWT '+ access
                        return axiosInstance(prevRequest)
                    }).catch(error=>console.log(error))
                    }else{
                        console.log("Refresh token has been expired")
                        localStorage.removeItem('user')
                        localStorage.setItem('isActive',false)
                    }
                }else{
                    console.log("Refresh token doesn't exist")
            }
        }
    }
)
export default axiosInstance