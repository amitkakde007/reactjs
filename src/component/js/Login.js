import React,{useState} from 'react'
import '../css/Login.css'
import {Link,useHistory} from 'react-router-dom'
import axiosInstance from './axios'
import { StateConsumer } from './StateProvider'

function Login() {
    const history = useHistory()
    const [,dispatch] = StateConsumer()
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    
    const signIn=(e)=>{
        e.preventDefault()
        axiosInstance.post('user/api/token/',{
            username:username,
            password:password,
        }).then(
            resp=>{
                const {access, refresh} = resp.data
                //const expiry = JSON.parse(atob(refresh.split('.')[1]))
                axiosInstance.defaults.headers['Authorization']= "JWT "+ access
                localStorage.setItem('access',access)
                localStorage.setItem('refresh',refresh)
                localStorage.setItem('user',username)
                localStorage.setItem('isActive',true)
                if(resp.data){
                    history.push('/home')
                    dispatch({
                        type:'SET_USER',
                        user:localStorage.getItem('user'),
                    })
                }else{
                    dispatch({
                        action:'SET_USER',
                        user:null
                    })
                }
            }).catch(error=>console.log(error))
    }

    return (
        <div className='login'>
            <Link to='/login'>
                <img className='login__logo' 
                    src='https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Amazon_logo.svg/1024px-Amazon_logo.svg.png' alt='' />
            </Link>
            <div className='login__container'>
                <h1>Login</h1>
                <form>
                    <h5>Username</h5>
                    <input className='input__email' type='text' value={username} onChange={(e)=>setUsername(e.target.value)}></input>

                    <h5>Password</h5>
                    <input className='input__email' type='password' value={password} onChange={(e)=>setPassword(e.target.value)}></input>
                    <button className='login__button' type='submit' onClick={signIn}>Log In</button>
                    <p><small>By continuing, you agree to Amazon's <a href='#top'>Conditions of use</a> and <a href='#top'>privacy Notice</a></small></p>
                </form>
            </div>
            <div className='divider'>
                <h5>New to Amazon?</h5>
                <p className='divider__line'></p>
                <p><button className='register__button'onClick={e=>history.push('/register')}>
                    Create your Amazon account</button></p>
            </div>
        </div>
    )
}

export default Login
